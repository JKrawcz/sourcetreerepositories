/*
 * i2c.h
 *
 * Created: 05/12/2019 19.34.23
 *  Author: JoannaKrawczyk
 */ 


#ifndef I2C_H_
#define I2C_H_


#include "gpio.h"

unsigned char i2c_setup(unsigned char type);
unsigned int i2c_transmit(unsigned int address, unsigned int data);
unsigned int i2c_read(unsigned int address,  unsigned int *data);


#endif /* I2C_H_ */