/*
 * led.c
 *
 * Created: 12/12/2019 19.31.02
 *  Author: JoannaKrawczyk
 */ 

#include "delay.h"

void ledBlink(void){
	
	for(int i = 0; i<3; i++){
	LED_On(RED_LED_FLEX);
	delay_ms(500);
	LED_Off(RED_LED_FLEX);
	delay_ms(500);
	}

}
