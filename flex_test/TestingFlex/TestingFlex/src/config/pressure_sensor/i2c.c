#include "i2c.h"
#include "press_conv.h"
#include "delay.h"

void i2c_init(bool *ready){
	TWBR = ((F_CPU/SCL_CLK)-16)/2;
	if (ready) *ready = ioport_get_pin_level(TWID_SDA);
}

unsigned char i2c_sendCommand(unsigned char type){
	//TWCR = (1 << TWIE);
	switch(type) {
		case START:			 // Send Start Condition (TWINT the interrupt flag), TWSTA (start condition bit), (TWEN enable bit)
			TWCR |= (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);
		break;
		case DATA_ACK:		//Send Data - with reading ack
			TWCR |= (1<<TWINT)|(1<<TWEN)|(1<<TWEA);
		break;
		case DATA_NACK:		//Send Data - with reading ack
			TWCR |= (1<<TWINT)|(1<<TWEN);
		break;
		case STOP:			// Send Stop Condition (TWSTO stop condition bit)
			TWCR |= (1 << TWINT) | (1 << TWEN) | (1 << TWSTO);
		return 0;
	}
	// Waiting for TWINT flag set in TWCR Register
	while (!(TWCR & (1 << TWINT)));
	// Returning TWI Status Register, mask the prescaler bits (TWPS1,TWPS0)
	uint8_t status = TWSR & 0xF8;
	return status;
}

unsigned int i2c_reset(void){
	
	unsigned char twi_status;
	char error = 0;

	// Transmit Start Condition
	twi_status=i2c_sendCommand(START);

	// Transmit I2C Data
	twi_status=i2c_write(DEV_ADDRESS);

	// Send the High 8-bit of I2C Address

	// Transmit I2C Data
	twi_status=i2c_write(RESET);
	
	delay_ms(3);
	
	// Check the TWSR status
	if (twi_status != RESET){
		error = 1;
		goto exit;
	}
	
	exit:
	twi_status=i2c_sendCommand(STOP);
		
	return error;
}

unsigned int i2c_begin(void)
{
	uint8_t buffer[3];
	uint8_t twi_status;
	char error;
	
	//==========READING MEMORY PART============//
	
	// Transmit Start Condition
	twi_status=i2c_sendCommand(START);
	// Send slave address (SLA_W)
	TWDR = write_device_address;
	// Transmit I2C Data
	twi_status=i2c_write(DEV_ADDRESS);
	//maybe needed reset once again
	for(int i = 0; i <= 7; i++)
	{
		twi_status = i2c_write(CMD_PROM + (i * 2));
		if (twi_status)
		{
			return twi_status;
		}
		twi_status=i2c_write(DEV_ADDRESS);					//to be checked
		twi_status = i2c_read_array(PROM_READ, &buffer, 2);  //to be checked PROM_READ
		if (twi_status)
		{
			//return twi_status;
		}
		
		coefficient[i] = (buffer[0] << 8)|buffer[1];
	}
	uint8_t crc_recv = (coefficient[0] & 0xF000) >> 12;
	uint8_t crc_calc =  MS5803_crc4(coefficient);
	if(crc_recv != crc_calc){
		error = 1;
	}
	twi_status=i2c_sendCommand(STOP);
	
	return error;
}

uint8_t i2c_startRead(unsigned char address){
	
	uint8_t twi_status;
	twi_status=i2c_sendCommand(START);
	// Transmit I2C Data
	twi_status=i2c_write(DEV_ADDRESS + READ);
	
	twi_status=i2c_write(address);
	return twi_status;
}

uint8_t i2c_startWrite(void){
	
	uint8_t twi_status;
	twi_status=i2c_sendCommand(START);
	// Transmit I2C Data
	twi_status=i2c_write(DEV_ADDRESS + WRITE);
	return twi_status;
}

unsigned char i2c_readAck(void){
	TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWEA);
	while(!(TWCR & (1<<TWINT)));    
    return TWDR;
}

unsigned char i2c_readNak(void)
{
	TWCR = (1<<TWINT) | (1<<TWEN);
	while(!(TWCR & (1<<TWINT)));
    return TWDR;
}

unsigned char i2c_write(unsigned char address)
{
	uint8_t error = 0;
	// send data to the previously addressed device
	TWDR = address;
	TWCR = (1<<TWINT) | (1<<TWEN);

	// wait until transmission completed
	while(!(TWCR & (1<<TWINT)));

	uint8_t status = TWSR & 0xF8;
	if (status != 0x28) error = 1;
	return error;
}

uint8_t i2c_read_array(uint8_t address, uint8_t * data, uint16_t length) //as twi_master_read
{
	uint16_t i;
	uint8_t twi_status;
	
	twi_status = i2c_startRead(address + READ); //check also without read

	for (i = 0; i < length-1; i++)
	{
		data[i] = i2c_readAck();
	}
	
	data[i] = i2c_readNak();
	twi_status = data[i];
	return twi_status;
}

uint8_t i2c_write_array(uint8_t address, uint8_t * data, uint16_t length) //
{
	uint8_t twi_status = 0;
	twi_status = i2c_startRead(address);
	
	for (uint16_t i = 0; i < length; i++)
	{
		twi_status = i2c_write(data[i]);
	}
	return twi_status;
} 

static uint32_t ms5803_pres;
static uint32_t ms5803_temp;

unsigned int i2c_read_data(void)
{
	uint8_t buffer[8];
	unsigned char twi_status;
	char error = 0;
	
	//====================================================== I2C command to initiate a pressure conversion (OSR = 2086 and type D1) ==================================================================//
	twi_status=i2c_sendCommand(START);
	twi_status=i2c_write(DEV_ADDRESS + WRITE);
	twi_status=i2c_write(CMD_ADC_CONV + PRESSURE);	//D1 CONV
	if (twi_status != 0x46)
	{
		error = 1;
		goto exit;
	}
	twi_status=i2c_sendCommand(STOP);
	delay_ms(100);
	
	//=================================================================== I2C read sequence and answer from MS5837 ===================================================================================//
	error = ADC_conv(&ms5803_pres);
	if (error == 1) goto exit;
	
	
	//=================================================================== I2C command to initiate a temp conversion ==================================================================================//
	twi_status=i2c_sendCommand(START);
	twi_status=i2c_write(DEV_ADDRESS + WRITE);
	twi_status=i2c_write(CMD_ADC_CONV + TEMPERATURE);
	if (twi_status != 0x56)
	{
		error = 1;
		goto exit;
	}	
	twi_status=i2c_sendCommand(STOP);
	delay_ms(100);	
	
	//==================================================================== I2C read sequence and answer from MS5837 ===================================================================================//
	error = ADC_conv(&ms5803_temp);
	if (error == 1) goto exit;
	
	//MS5803_convertMeasurements();
	
	exit:
	twi_status=i2c_sendCommand(STOP);
	return error;
}

unsigned int ADC_conv(uint32_t *conversion)
{
	//unsigned int * conversion = 0; //to be defined
		unsigned int twi_status;

		uint8_t buffer[2];
		uint32_t result;
		
		twi_status=i2c_sendCommand(START);

		twi_status=i2c_write(DEV_ADDRESS + READ);
		
		// Transmit I2C Data
		//twi_status=i2c_write(CMD_ADC_READ);

		// Read I2C Data
		twi_status=i2c_read_array(CMD_ADC_READ, buffer, 3);	//D1 CONV
	
		twi_status=i2c_sendCommand(STOP);
		
		result = ((uint32_t)buffer[0] << 16) + ((uint32_t)buffer[1] << 8) + buffer[2];

	if (conversion) *conversion = result;
		
	return twi_status;	
}

static uint8_t MS5803_crc4(unsigned int n_prom[]) // n_prom defined as 8x unsigned int (n_prom[8])
{
	int cnt; // simple counter
	uint16_t n_rem=0; // crc remainder
	unsigned char n_bit;
	n_prom[0]=((n_prom[0]) & 0x0FFF); // CRC byte is replaced by 0
	n_prom[7]=0; // Subsidiary value, set to 0
	for (cnt =0; cnt < 16; cnt++) // operation is performed on bytes
	{ // choose LSB or MSB
		if (cnt%2==1) n_rem ^= (unsigned short) ((n_prom[cnt>>1]) &0x00FF);
		else n_rem ^= (unsigned short) (n_prom[cnt>>1]>>8);
		for (n_bit = 8; n_bit > 0; n_bit--)
		{
			if (n_rem & (0x8000)) n_rem = (n_rem << 1) ^ 0x3000;
			else n_rem = (n_rem << 1);
		}
	}
	n_rem= ((n_rem >> 12) & 0x000F); // final 4-bit remainder is CRC code
	return (n_rem ^ 0x00);
}
