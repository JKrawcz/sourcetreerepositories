/*
 * i2c.h
 *
 * Created: 05/12/2019 19.34.23
 *  Author: JoannaKrawczyk
 */ 

#ifndef I2C_H_
#define I2C_H_
#include "gpio.h"
#include "press_conv.h"

#define SCL_CLK  400000L

#define WRITE 0
#define READ 1

#define START			0
#define DATA_NACK		2
#define DATA_ACK		1
#define STOP			3

#define ACK				0
#define NACK			1

#define write_device_address	0xEC
#define read_device_address		0xED

#define DEV_ADDRESS		0x76
#define ADDR			0x76
#define RESET			0x1E
#define PROM_READ		0xA6
#define D1_CONV			0x48	//for pressure
#define D2_CONV			0x58	//for temp
#define CMD_ADC_PRESS	0x00
#define CMD_ADC_CONV	0x46	//osr already set for 2048

uint16_t coefficient[8];
uint32_t ms5803_temperature_actual;
uint32_t ms5803_pressure_actual;



void				i2c_init(bool *ready);
uint8_t				i2c_startRead(unsigned char address);
uint8_t				i2c_read_array(uint8_t address, uint8_t * data, uint16_t length);
uint8_t				i2c_write_array(uint8_t address, uint8_t * data, uint16_t length);
unsigned char		i2c_write(unsigned char address);
unsigned char		i2c_readAck(void);
unsigned char		i2c_readNak(void);
unsigned int		i2c_begin(void);
unsigned int		i2c_reset(void);
unsigned int		ADC_conv(uint32_t *conversion);
unsigned int		i2c_prom_read(void);
unsigned char		i2c_sendCommand(unsigned char type);
unsigned int		i2c_read_data(void);
static uint8_t		MS5803_crc4(uint16_t n_prom[]); 
uint8_t				i2c_startWrite(void);
#endif /* I2C_H_ */