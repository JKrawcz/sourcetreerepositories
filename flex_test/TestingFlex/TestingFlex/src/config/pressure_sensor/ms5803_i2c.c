/*
 * ms5803_i2c.c
 *
 * Created: 06/12/2019 14.19.54
 *  Author: JoannaKrawczyk
 */ 
#include <asf.h>
#include "MS5803_I2C.h"

#include "util.h"
//#include "leds.h"
#include "press_conv.h"

#include <wireless/fs.h>
#include <wireless/fscfg.h>
#include <wireless/fserr.h>
#include <wireless/timer.h>

uint32_t ms5803_temperature_actual;
uint32_t ms5803_pressure_actual;

ms5803_addr _address;     // Variable used to store I2C device address.
uint16_t coefficient[8];// Coefficients;

#ifndef TWIM0
#define TWIM0 TWI3
#endif /* TWIM0 */

/** States */
enum
{
	MS5803_STATE_IDLE,
	MS5803_STATE_WAIT_PRES,
	MS5803_STATE_WAIT_TEMP
};

/** Current state */
static uint8_t ms5803_state = MS5803_STATE_IDLE;

/** Current timer */
static t_timer *ms5803_timer = NULL;

/** Current callback to client */
static t_ms5803_cb ms5803_cb = NULL;

/** Current precision of conversion */
static t_ms5803_precision ms5803_precision;

static uint8_t ms5803_timer_cb(void* ref);

static uint8_t MS5803_getADCconversion(uint32_t *conversion);

uint8_t util_flush_i2c_clock(bool *ready)
{
	uint8_t flush_timeout=100;

	if (ioport_get_pin_level(I2C_SDA) == 0) {
		ioport_set_pin_dir(I2C_SCK, IOPORT_DIR_INPUT);
		ioport_enable_pin(I2C_SCK);

		while((ioport_get_pin_level(I2C_SDA) == 0) && flush_timeout)
		{
			/* pull low */
			ioport_set_pin_dir(I2C_SCK, IOPORT_DIR_OUTPUT);
			ioport_set_pin_level(I2C_SCK, false);
			delay_us(2);

			/* let phy pull-up pull high */
			ioport_set_pin_dir(I2C_SCK, IOPORT_DIR_INPUT);
			delay_us(3);

			/* check sda level */
			if (ioport_get_pin_level(I2C_SDA)) {
				break;
			}

			flush_timeout--;
		}
		ioport_set_pin_dir(I2C_SCK, IOPORT_DIR_INPUT);
		ioport_set_pin_level(I2C_SCK, false);
		ioport_disable_pin(I2C_SCK);
	}

	if (ready) *ready = ioport_get_pin_level(I2C_SDA);

	return FS_ERR_OK;
}



uint8_t MS5803_Init(bool *present)
{
	uint8_t err;
	bool ready;

	/* Flush the state machine on the chip if it is stuck */
	err = util_flush_i2c_clock(&ready);
	if(err)
	{
		return err;
	}
	if(!ready)
	{
		return err;
	}
	
	if(ms5803_timer)
	{
		err = timer_stop(ms5803_timer);
		FSERR_RETURN(err);
		
		ms5803_timer = NULL;
	}

	ms5803_cb = NULL;
	_address = ADDRESS_HIGH;

	err = MS5803_reset();
	if (fserr_pop() != FS_ERR_OK) {
		if (present) *present = false;
		return FS_ERR_OK;
	}

	err = MS5803_begin();
	if (fserr_pop() != FS_ERR_OK) {
		if (present) *present = false;
		return FS_ERR_OK;
	}
	
	if (present) *present = true;

	return FS_ERR_OK;
}

static uint8_t MS5803_sendCommand(uint8_t command)
{
	uint8_t err;
	bool ready;
	const uint8_t buffer[1] = {command};

	twi_package_t packet_write = {
		.addr         = {0},      // TWI slave memory address data
		.addr_length  = 0,    // TWI slave memory address data size
		.chip         = _address,      // TWI slave bus address
		.buffer       = (void *)buffer, // transfer data source buffer
		.length       = 1  // transfer data size (bytes)
	};

	err = twi_master_write(TWIM0, &packet_write);
	if(err != STATUS_OK)
	{
		/* Try to flush the clock to get the device back on track */
		err = util_flush_i2c_clock(&ready);
		if(err)
		{
			return err;
		}
		if(!ready)
		{
			return 1;
		}

		/* Try again */
		err = twi_master_write(TWIM0, &packet_write);
		if(err != STATUS_OK)
		{
			return 1;
		}
	}

	return 0;
}

static uint8_t MS5803_readData(uint8_t * buffer, uint8_t len)
{
	uint8_t err;
	bool ready;
	twi_package_t packet_read = {
		.addr         = {0},      // TWI slave memory address data
		.addr_length  = 0,    // TWI slave memory address data size
		.chip         = _address,      // TWI slave bus address
		.buffer       = buffer,        // transfer data destination buffer
		.length       = len                    // transfer data size (bytes)
	};

	err = twi_master_read(TWIM0, &packet_read);
	if(err != STATUS_OK)
	{
		/* Try to flush the clock to get the device back on track */
		err = util_flush_i2c_clock(&ready);
		if(err)
		{
			return err;
		}
		if(!ready)
		{
			return 1;
		}

		/* Try again */
		err = twi_master_read(TWIM0, &packet_read);
		if(err != STATUS_OK)
		{
			return 1;
		}
	}
	return FS_ERR_OK;
}

uint8_t MS5803_reset(void)
{
	uint8_t err;

	/* Abort timer */
	if(ms5803_timer)
	{
		err = timer_stop(ms5803_timer);
		FSERR_RETURN(err);

		ms5803_timer = NULL;
	}

	/* Send the reset command */
	err = MS5803_sendCommand(CMD_RESET);
	if (err)
	{
		return err;
	}

	delay_ms(3);

	ms5803_state = MS5803_STATE_IDLE;
	return FS_ERR_OK;
}

static uint8_t MS5803_crc4(uint16_t n_prom[]) // n_prom defined as 8x unsigned int (n_prom[8])
{
	int cnt; // simple counter
	uint16_t n_rem=0; // crc remainder
	unsigned char n_bit;
	n_prom[0]=((n_prom[0]) & 0x0FFF); // CRC byte is replaced by 0
	n_prom[7]=0; // Subsidiary value, set to 0
	for (cnt =0; cnt < 16; cnt++) // operation is performed on bytes
	{ // choose LSB or MSB
		if (cnt%2==1) n_rem ^= (unsigned short) ((n_prom[cnt>>1]) &0x00FF);
		else n_rem ^= (unsigned short) (n_prom[cnt>>1]>>8);
		for (n_bit = 8; n_bit > 0; n_bit--)
		{
			if (n_rem & (0x8000)) n_rem = (n_rem << 1) ^ 0x3000;
			else n_rem = (n_rem << 1);
		}
	}
	n_rem= ((n_rem >> 12) & 0x000F); // final 4-bit remainder is CRC code
	return (n_rem ^ 0x00);
}

uint8_t MS5803_begin(void)
// Initialize library for subsequent pressure measurements
{
	uint8_t err;
	uint8_t buffer[3];
	uint8_t i;

	#if (MODEL != MS5803_05BA)
	for(i = 0; i <= 6; i++)
	#else
	for(i = 0; i <= 7; i++)
	#endif
	{
		err = MS5803_sendCommand(CMD_PROM + (i * 2));
		if (err)
		{
			return err;
		}
		
		err = MS5803_readData(buffer, 2);
		if (err)
		{
			return err;
		}
		
		coefficient[i] = (buffer[0] << 8)|buffer[1];
	}
	
	/* Calculate crc for factory calibration */
	uint8_t crc_recv = (coefficient[0] & 0xF000) >> 12;
	uint8_t crc_calc =  MS5803_crc4(coefficient);
	
	if(crc_recv != crc_calc)
	{
		/* calibration wiped :( */
		return 1;
	}
	

	return 0;
}


static uint8_t MS5803_getADCconversion(uint32_t *conversion)
{
	uint8_t err;
	uint32_t result;
	uint8_t buffer[3];

	err = MS5803_sendCommand(CMD_ADC_READ);
	if (err)
	{
		return err;
	}

	err = MS5803_readData(buffer, 3);
	if (err)
	{
		return err;
	}

	result = ((uint32_t)buffer[0] << 16) + ((uint32_t)buffer[1] << 8) + buffer[2];

	if (conversion) *conversion = result;
	return FS_ERR_OK;
}

static uint8_t ms5803_start_int(t_ms5803_measurement measurement)
{
	uint8_t err;
	uint8_t time;

	/* Start conversion of D1 - pressure */
	err = MS5803_sendCommand(CMD_ADC_CONV + measurement + ms5803_precision);
	if(err)
	{
		return err;
	}

	switch(ms5803_precision)
	{
		default:
		case ADC_256 : time = 2; break;
		case ADC_512 : time = 4; break;
		case ADC_1024: time = 6; break;
		case ADC_2048: time = 7; break;
		case ADC_4096: time = 11; break;
		#if (MODEL != MS5803_05BA)
		case ADC_8192: time = 20; break;
		#endif
	}

	if(ms5803_timer)
	{
		FSERR_RETURN(FS_ERR_FAILED);
	}

	err = timer_start(time, ms5803_timer_cb, NULL, &ms5803_timer);
	FSERR_RETURN(err);

	return FS_ERR_OK;
}

static uint8_t ms5803_timer_cb_int(void)
{
	uint8_t err;

	if(ms5803_state == MS5803_STATE_WAIT_PRES)
	{
		ms5803_pres = 0;
		err = MS5803_getADCconversion(&ms5803_pres);
		FSERR_RETURN(err);

		if (ms5803_pres == 0)
		{
			return 1;
		}

		/* Now convert D2 - temp */
		err = ms5803_start_int(TEMPERATURE);
		if (err)
		{
			return err;
		}

		ms5803_state = MS5803_STATE_WAIT_TEMP;
	}
	else if(ms5803_state == MS5803_STATE_WAIT_TEMP)
	{
		ms5803_temp = 0;
		err = MS5803_getADCconversion(&ms5803_temp);
		if (err)
		{
			return err;
		}

		if (ms5803_temp == 0) {
			return 1;
		}

		/* Convert result */
		MS5803_getMeasurements();

		ms5803_state = MS5803_STATE_IDLE;

		if(!ms5803_cb)
		{
			return 1;
		}
		
		err = ms5803_cb(ms5803_pressure_actual, ms5803_temperature_actual, FS_ERR_OK);
		FSERR_RETURN(err);
		
		ms5803_cb = NULL;
	}

	return FS_ERR_OK;
}

static uint8_t ms5803_timer_cb(void* ref)
{
	uint8_t err;
	
	/* Clear timer */
	ms5803_timer = NULL;
	
	err = ms5803_timer_cb_int();
	if(fserr_pop() != FS_ERR_OK)
	{
		if(ms5803_cb)
		{
			err = ms5803_cb(0, 0, FS_ERR_FAILED);
			FSERR_RETURN(err);
			
			ms5803_cb = NULL;
			
			ms5803_state = MS5803_STATE_IDLE;
		}
	}
	
	return FS_ERR_OK;
}

uint8_t ms5803_start(t_ms5803_cb cb, t_ms5803_precision precision)
{
	uint8_t err;

	#if (LED_CFG_USAGE_MASK & LED_IND_PRES)
	err = leds_start_indication(LEDS_INDI_BLUEGREEN_FLASH_ONCE);
	FSERR_RETURN(err);
	#endif

	/* check if reading sequence has been started already */
	if(ms5803_state != MS5803_STATE_IDLE)
	{
		return FS_ERR_OK;
	}

	/* Store precision */
	ms5803_precision = precision;

	/* Start conversion of D1 - pressure */
	err = ms5803_start_int(PRESSURE);
	if(err)
	{
		return err;
	}

	/* Update state */
	ms5803_state = MS5803_STATE_WAIT_PRES;

	/* Save callback */
	ms5803_cb = cb;
	return FS_ERR_OK;
}
