#include <stdint-gcc.h>
#include <stdbool.h>
#include "press_conv.h"
#include "i2c.h"

/*
 * press_conv.c
 *
 * Created: 06/12/2019 11.43.21
 *  Author: JoannaKrawczyk
 */ 

ms5803_addr _address;     // Variable used to store I2C device address.
uint16_t coefficient[8];// Coefficients;

uint32_t temp_actual;
uint32_t pres_actual;
static uint32_t ms5803_pres;
static uint32_t ms5803_temp;

static void MS5803_convertMeasurements(void)
{
	//Retrieve ADC result
	uint32_t D1 = ms5803_pres;
	uint32_t D2 = ms5803_temp;

	//Create Variables for calculations
	int64_t temp_calc;
	int64_t pressure_calc;

	int32_t dT;
	
	/* Model MS5837-02 or MS5837-30 */
	#if (MODEL != MS5803_05BA)
	int64_t SENS = 0;
	int64_t OFF = 0;
	int32_t SENSi = 0;
	int32_t OFFi = 0;
	int32_t Ti = 0;
	int64_t OFF2 = 0;
	int64_t SENS2 = 0;
	
	// Terms called
	dT = D2 - ((uint32_t)coefficient[5] * 256l);
	#if (MODEL == MS5837_02BA)
	SENS = ((int64_t)coefficient[1]*65536l)+((int64_t)coefficient[3]*dT/128l);
	OFF = ((int64_t)coefficient[2]*131072l)+((int64_t)coefficient[4]*dT/64l);
	pressure_calc = (D1*SENS/(2097152l)-OFF)/(32768l);
	#else
	SENS = ((int64_t)coefficient[1]*32768l)+((int64_t)coefficient[3]*dT/256l);
	OFF = ((int64_t)coefficient[2])*65536l+((int64_t)coefficient[4]*dT/128l);
	pressure_calc = (D1*SENS/(2097152l)-OFF)/(8192l);
	#endif
	
	// Temp conversion
	temp_calc = 2000l+((int64_t)dT*coefficient[6]/8388608LL);
	
	//Second order compensation
	#if (MODEL == MS5837_02BA)
	if((temp_calc/100)<20){         //Low temp
		Ti = (11*((int64_t)dT*dT))/(34359738368LL);
		OFFi = (31*(temp_calc-2000)*(temp_calc-2000))/8;
		SENSi = (63*(temp_calc-2000)*(temp_calc-2000))/32;
	}
	#else
	if((temp_calc/100)<20) {         //Low temp
		Ti = (3*((int64_t)dT*dT))/(8589934592LL);
		OFFi = (3*(temp_calc-2000)*(temp_calc-2000))/2;
		SENSi = (5*(temp_calc-2000)*(temp_calc-2000))/8;
		if((temp_calc/100)<-15) {    //Very low temp
			OFFi = OFFi+7*(temp_calc+1500l)*(temp_calc+1500l);
			SENSi = SENSi+4*(temp_calc+1500l)*(temp_calc+1500l);
		}
	}
	else if((temp_calc/100)>=20){    //High temp
		Ti = 2*(dT*dT)/(137438953472LL);
		OFFi = (1*(temp_calc-2000)*(temp_calc-2000))/16;
		SENSi = 0;
	}
	#endif
	
	OFF2 = OFF-OFFi;           //Calculate pressure and temp second order
	SENS2 = SENS-SENSi;
	
	#if (MODEL == MS5837_02BA)
	temp_calc = (temp_calc-Ti);
	pressure_calc = (int64_t)(((D1*SENS2)/2097152l-OFF2)*10/32768l);
	#else
	temp_calc = (temp_calc-Ti);
	pressure_calc = (int64_t)(((D1*SENS2)/2097152l-OFF2)*10/8192l);
	#endif

	#else /* Model MS5803-05 */

	// Now that we have a raw temperature, let's compute our actual.
	dT = D2 - ((int32_t)coefficient[5] << 8);
	temp_calc = (((int64_t)dT * coefficient[6]) >> 23) + 2000;

	//Now we have our first order Temperature, let's calculate the second order.
	int64_t T2, OFF2, SENS2, OFF, SENS; //working variables

	if (temp_calc < 2000)
	// If temp_calc is below 20.0C
	{
		T2 = 3 * (((int64_t)dT * dT) >> 33);
		OFF2 = 3 * ((temp_calc - 2000) * (temp_calc - 2000)) / 2;
		SENS2 = 5 * ((temp_calc - 2000) * (temp_calc - 2000)) / 8;

		if(temp_calc < -1500)
		// If temp_calc is below -15.0C
		{
			OFF2 = OFF2 + 7 * ((temp_calc + 1500) * (temp_calc + 1500));
			SENS2 = SENS2 + 4 * ((temp_calc + 1500) * (temp_calc + 1500));
		}
	}
	else
	// If temp_calc is above 20.0C
	{
		T2 = 0;
		OFF2 = 0;
		SENS2 = 0;
	}

	OFF = ((int64_t)coefficient[2] << 18) + (((coefficient[4] * (int64_t)dT)) >> 5);  //C2 *2^18 +(C4 *dT ) / 2^5
	SENS = ((int64_t)coefficient[1] << 17) + (((coefficient[3] * (int64_t)dT)) >> 7); //C1 * 2^17 + (C3 * dT ) / 2^7

	temp_calc = temp_calc - T2;
	OFF = OFF - OFF2;
	SENS = SENS - SENS2;
	
	pressure_calc = (((SENS * D1) / 2097152 ) - OFF) / 32768; //(D1 * SENS / 2^21 - OFF) / 2^15
	#endif

	/* Store temperature in mC */
	temp_actual = temp_calc * 10;
	/* Store pressure in ubar */
	pres_actual = pressure_calc * 10;
}