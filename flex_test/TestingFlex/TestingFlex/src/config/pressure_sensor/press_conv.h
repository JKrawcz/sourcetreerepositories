/*
 * press_conv.h
 *
 * Created: 06/12/2019 11.43.08
 *  Author: JoannaKrawczyk
 */ 

#ifndef PRESS_CONV_H_
#define PRESS_CONV_H_

#define MS5803_05BA     0
#define MS5837_30BA     1
#define MS5837_02BA     2

#define MODEL           MS5837_30BA 

// Define units for conversions.
typedef enum
{
  CELSIUS,
  FAHRENHEIT,
}temperature_units;

// Define measurement type.
typedef enum
{
  PRESSURE = 0x00,
  TEMPERATURE = 0x10
}t_ms5803_measurement;

// Define constants for Conversion precision
typedef enum
{
  ADC_256  = 0x00,
  ADC_512  = 0x02,
  ADC_1024 = 0x04,
  ADC_2048 = 0x06,
  ADC_4096 = 0x08,
#if (MODEL != MS5803_05BA)
  ADC_8192 = 0x0A,
#endif  
}t_ms5803_precision;

// Define address choices for the device (I2C mode)
typedef enum
{
  ADDRESS_HIGH = 0x76,
  ADDRESS_LOW  = 0x77
}ms5803_addr;

//Commands
#define CMD_RESET 0x1E // reset command
#define CMD_ADC_READ 0x00 // ADC read command
#define CMD_ADC_CONV 0x40 // ADC conversion command

#define CMD_PROM 0xA0 // Coefficient location

uint8_t MS5803_Init(bool *present);
uint8_t MS5803_reset(void);   //Reset device
uint8_t MS5803_begin(void); // Collect coefficients from sensor
/** Callback for conversion
  */
typedef uint8_t (*t_ms5803_cb)(uint32_t pres, uint32_t temp, uint8_t status);

/** Start a new conversion
  * @param[in] cb Callback when conversion is done
  * @param[in] precision Precision to convert at
  *
  * @return Status
  */



#endif /* PRESS_CONV_H_ */