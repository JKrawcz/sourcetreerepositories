#include <asf.h>
#include <board.h>
#include <compiler.h>
#include <conf_board.h>
#include <avr/io.h>
#include <stdint-gcc.h>
#include <avr/interrupt.h>
#include "gpio.h"
#include "LED/led.h"
#include "pressure_sensor/i2c.h"
#include "pressure_sensor/press_conv.h"
#include "usart_mega.h"

//#include "i2c.h"

void board_init(void)
{
	/* On board LED initialization */
	ioport_configure_pin(RED_LED_FLEX, IOPORT_DIR_OUTPUT |  IOPORT_INIT_HIGH);
	ioport_configure_pin(LED0, IOPORT_DIR_OUTPUT |  IOPORT_INIT_HIGH);
	
	/* On board Switch initialization */
	ioport_configure_pin(GPIO_PUSH_BUTTON_0,IOPORT_DIR_INPUT | IOPORT_PULL_UP);

	//ioport_configure_pin(TWID_SDA, IOPORT_DIR_OUTPUT |IOPORT_INIT_HIGH);
	//ioport_configure_pin(TWID_SCL, IOPORT_DIR_OUTPUT |IOPORT_INIT_HIGH);

	ioport_configure_pin(TWID_SDA,IOPORT_DIR_INPUT | IOPORT_PULL_UP);
	ioport_configure_pin(TWID_SCL,IOPORT_DIR_INPUT | IOPORT_PULL_UP);

    delay_us(2);

}

int main (void)
{
	bool ready;
	bool button_state;
	
	unsigned int resetErr, measureErr, beginErr;

	board_init();
	UART_init(19200, 8, 1);
	i2c_init(&ready);
	
	sei();
	
	ledBlink();
	
	resetErr = i2c_reset();
	delay_ms(100);
	beginErr = i2c_begin();
	delay_ms(100);
	measureErr = i2c_read_data();
		
	while(1){
		button_state = ioport_get_pin_level(GPIO_PUSH_BUTTON_0);

		if(button_state){
		LED_Off(LED0);
		}else{
		LED_Off(LED0);
		}
	}	
}
