
#include "fs.h"
#include "fserr.h"
#include "timer.h"
#include "timerio.h"

typedef struct _t_timer
{
	uint32_t timeout;
	uint8_t busy;
	t_timer_cb cb;
	void *ref;
}t_timer;

#define TIMER_LIST_MAX          20
#define TIMER_INDEX_INVALID     0xFF

static t_timer timer_list[TIMER_LIST_MAX] = {{0}};
static bool timer_driver_started = false;

uint32_t main_tick_get(void);
uint32_t timer_tick_get(void)
{
	uint8_t err;
	uint32_t ticks;

	err = timerio_tick_get(&ticks);
	if(err)
	{
		FSERR_PANIC(err);
	}

	return ticks;
}

static uint8_t timer_alloc(t_timer **timer)
{
	uint8_t i;
	for(i=0;i<TIMER_LIST_MAX;i++)
	{
		if(!timer_list[i].busy)
		{
			*timer = &timer_list[i];
			return FS_ERR_OK;
		}
	}

	FSERR_RETURN(FS_ERR_FAILED);
}

uint8_t timer_sleep(uint32_t ms)
{
	uint8_t err;

	err = timerio_sleep(ms);
	FSERR_RETURN(err);

	return FS_ERR_OK;
}

static uint8_t timer_next_get(t_timer **timer)
{
	uint32_t timeout = 0xFFFFFFFF;
	uint8_t i;
	*timer = NULL;

	/* Find smallest timer */
	for(i=0;i<TIMER_LIST_MAX;i++)
	{
		if(timer_list[i].busy && (timer_list[i].timeout < timeout))
		{
			*timer = &timer_list[i];
			timeout = timer_list[i].timeout;
		}
	}

	return FS_ERR_OK;
}

static uint8_t timer_start_driver(void)
{
	t_timer *timer;
	uint8_t err;
	if(!timer_driver_started)
	{
		err = timer_next_get(&timer);
		FSERR_RETURN(err);

		/* Start next timer */
		if(timer)
		{
			err = timerio_start(timer->timeout);
			FSERR_RETURN(err);

			/* Remember driver is running */
			timer_driver_started = true;
		}
	}
	return FS_ERR_OK;
}

static uint8_t timer_stop_driver(void)
{
	uint8_t err;

	if(timer_driver_started)
	{
		err = timerio_stop();
		FSERR_RETURN(err);

		timer_driver_started = false;
	}
	return FS_ERR_OK;
}

uint8_t timer_start(uint32_t time, t_timer_cb cb, void *ref, t_timer **timer)
{
	uint8_t err;
	t_timer *timer_new=NULL, *timer_next;

	/* check timer variable */
	FSERR_ASSERT(timer);
	FSERR_ASSERT(!(*timer));
	FSERR_ASSERT(time);

	#if 0
	/* check that same call back is not used twice */
	for(uint8_t i = 0;i<TIMER_LIST_MAX;i++)
	{
		if(timer_list[i].busy && (timer_list[i].cb == cb))
		{
			FSERR_RETURN(FS_ERR_FAILED);
		}
	}
	#endif

	/* Allocate new timer */
	err = timer_alloc(&timer_new);
	FSERR_RETURN(err);
	timer_new->timeout = timer_tick_get() + time;
	timer_new->ref = ref;
	timer_new->cb = cb;
	timer_new->busy = 1;

	*timer = timer_new;

	/* Check if we need to reprogram timer */
	err = timer_next_get(&timer_next);
	FSERR_RETURN(err);
	if(timer_next == timer_new)
	{
		err = timer_stop_driver();
		FSERR_RETURN(err);

		err = timer_start_driver();
		FSERR_RETURN(err);
	}

	return FS_ERR_OK;
}

uint8_t timer_stop(t_timer *timer)
{
	uint8_t err, i;
	t_timer *timer_next;

	/* check timer variable */
	FSERR_ASSERT(timer);
	FSERR_ASSERT(timer->busy)

	err = timer_next_get(&timer_next);
	FSERR_RETURN(err);
	if(timer_next == timer)
	{
		err = timer_stop_driver();
		FSERR_RETURN(err);
	}

	/* Check if this was actually a timer */
	for(i=0;i<TIMER_LIST_MAX;i++)
	{
		if(&timer_list[i] == timer)
		{
			break;
		}
	}
	if(i == TIMER_LIST_MAX)
	{
		FSERR_RETURN(FS_ERR_FAILED);
	}

	/* Just mark it unused */
	timer->busy = 0;

	err = timer_start_driver();
	FSERR_RETURN(err);
	return FS_ERR_OK;
}

uint8_t timer_poll(void)
{
	uint8_t i, err;

	/* Make sure timer is stopped */
	err = timer_stop_driver();
	FSERR_RETURN(err);

	uint32_t time_now = timer_tick_get();

	for(i=0;i<TIMER_LIST_MAX;i++)
	{
		if(timer_list[i].busy && (timer_list[i].timeout <= time_now))
		{
			/* Free up the timer */
			timer_list[i].busy = 0;

			/* Call client */
			err = timer_list[i].cb(timer_list[i].ref);
			if(err)
			{
				return err;
			}
		}
	}

	/* Restart timer if necessary */
	err = timer_start_driver();
	FSERR_RETURN(err);

	return FS_ERR_OK;
}

uint8_t timer_init(void)
{
	uint8_t err;
	memset(timer_list,0,sizeof(timer_list));
	timer_driver_started = false;

	err = timerio_init();
	FSERR_RETURN(err);

	return FS_ERR_OK;
}
