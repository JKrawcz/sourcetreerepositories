

#ifndef TIMER_H_
#define TIMER_H_
/** timer reference */
typedef struct _t_timer t_timer;

/** Timer callback */
typedef uint8_t (*t_timer_cb)(void *ref);

/** Get the current timer tick
  *
  * @return Current timer ticks in ms
  */
FS_EXPORT1 uint32_t timer_tick_get(void);

/** Start a timer
  * @param[in] time Time from now to expire
  * @param[in] cb Callback to call when timer expires
  * @param[in] ref Callback reference
  * @param[out] timer Timer handle
  *
  * @return Status
  */
uint8_t timer_start(uint32_t time, t_timer_cb cb, void *ref, t_timer **timer);

/** Stop a timer
  * @param[in] timer Timer to stop
  *
  * @return Status
  */
uint8_t timer_stop(t_timer *timer);

/** Intialize module and reset all timers
  *
  * @return Status
  */
uint8_t timer_init(void);

/** Poll the timer from the main loop
  *
  * @return Status
  */
FS_EXPORT1  uint8_t timer_poll(void);

/** Sleep synchroniously for a number of milliseconds
  *
  * @return Status
  */
uint8_t timer_sleep(uint32_t ms);





#endif /* TIMER_H_ */