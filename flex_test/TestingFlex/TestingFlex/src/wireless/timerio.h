/*
 * timerio.h
 *
 * Created: 06/12/2019 14.26.34
 *  Author: JoannaKrawczyk
 */ 


#ifndef TIMERIO_H_
#define TIMERIO_H_

/****************************************************************
*  _      ___
* | \ _  _ | _  _|_
* |_/(_)| ||(/_(_| | ApS
*
* For Freesense ApS
*
* (C)Copyright 2016 Freesense ApS
*
***************************************************************
* @author Peter Dons Tychsen (pdt@dontech.dk)
*
* @ingroup timerio
* @file timerio.h
*
* @brief Timer driver declarations
*
*/

/** Initialize driver
  *
  * @return Status
  */
uint8_t timerio_init(void);

/** Sleep for some time synchroniously
  * @param[in] ms Milliseconds to sleep
  *
  * @return Status
  */
uint8_t timerio_sleep(uint32_t ms);

/** Sleep for some time asynchronously
  * @param[in] ms Milliseconds to sleep
  *
  * @return Status
  */
uint8_t timerio_start(uint32_t ms);

/** Stop the driver timer
  *
  * @return Status
  */
uint8_t timerio_stop(void);

/** Get the current tick count from the system
  * @param[out] ticks Number of ticks passed since boot
  *
  * @return Status
  */
uint8_t timerio_tick_get(uint32_t *ticks);




#endif /* TIMERIO_H_ */